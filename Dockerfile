FROM openjdk:11
ADD target/docker-component-gateway-zuul.jar docker-component-gateway-zuul.jar
EXPOSE 8000
ENTRYPOINT ["java", "-jar", "docker-component-gateway-zuul.jar"]