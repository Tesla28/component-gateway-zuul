package com.quot.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
public class ComponentGatewayZuulApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComponentGatewayZuulApplication.class, args);
	}

}
